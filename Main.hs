module Main where

import Control.Exception (throwIO, try)
import Control.Lens ((^.))
import Control.Monad (filterM, mapM_)
import Data.List (concatMap, minimumBy, permutations)
import Data.Traversable (for)
import Network.HTTP.Client (HttpException(..))
import Network.Wreq (FormParam((:=)), post, responseStatus, statusCode)
import qualified Network.Wreq.Session as S
import System.IO (IOMode(WriteMode), hPutStrLn, withFile)

firstName = "jacob"
middleName = "levi"
lastName = "walker"

nameParts :: [[String]]
nameParts = permutations [firstName, lastName] ++ permutations [firstName, middleName, lastName]

main :: IO ()
main = S.withSession $ \sess -> do
  let possibleOrderedCombos = concatMap orderedBalancedPiecesCombos nameParts
  availableNames <- filterM (githubUsernameAvailable sess) possibleOrderedCombos
  mapM_ putStrLn availableNames
  withFile "usernames.txt" WriteMode $ \hdl -> mapM_ (hPutStrLn hdl) availableNames

orderedBalancedPiecesCombos :: [String] -> [String]
orderedBalancedPiecesCombos strings = concatMap (\parts -> orderedBalancedCombos [1..shortestLength parts] parts) stringsCombos
  where stringsCombos = foldr (\count l -> map (drop count) strings : l) [] [0..(shortestLength strings - 1)] -- [["jacob", "walker"], ["acob", "alker"], ["cob", "lker"], ..]

-- take ["jacob", "walker"] and get ["jw", "jawa", "jacwal", ..]
-- take ["acob", "alker"] and get ["aa", "acal", "acoalk", ..]
orderedBalancedCombos :: [Int] -- ^ chunk sizes to use
                      -> [String] -- ^ strings to chunk and combine
                      -> [String]
orderedBalancedCombos pieceSizes strings =
  foldr (\s l -> concatMap (take s) strings : l) [] pieceSizes

shortestLength :: [[a]] -> Int
shortestLength = length . minimumBy (\a b -> if length a > length b then GT else LT)

githubUsernameAvailable :: S.Session -> String -> IO Bool
githubUsernameAvailable sess name = do
  r <- try $ S.post sess "https://github.com/signup_check/username" ["value" := name]
  case r of
       Left e@(StatusCodeException s _ _) -> return False
       Left e -> throwIO e
       Right res -> return $ res ^. responseStatus . statusCode == 200
